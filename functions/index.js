const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
const express = require('express');
const exphbs = require('express-handlebars');
const app = express();
const botDetect = require('./botDetect.js');
const appUrl = 'rauquagiamcan.firebaseapp.com';

app.engine('handlebars', exphbs({
  defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

app.get('*', (req, res) => {
  const isBot = botDetect.detectBot(req.headers['user-agent']);
  var url = req.protocol + '://' + appUrl + req.originalUrl;
  if (isBot) {

    var params = req.originalUrl.split('/');
    var doc = params[2];

    admin.firestore().collection('products').doc(doc).get().then(snapshot => {
      var data = snapshot.data();
      res.render('index', {
        title: data.title,
        description: data.description,
        url: url,
        image: data.photoUrl
      });
    })
  } else {
    res.render('index');
  }

});

exports.app = functions.https.onRequest(app);

exports.sendNotification = functions.firestore
  .document('/carts/{value}').onCreate((event) => {
    // ... Your code here
    const data = event.data.data();
    const payload = {
      notification: {
        title: 'Có đơn hàng mới',
        body: 'Bạn ' + data.user + ' ở ' + data.address,
        icon: "https://lh6.googleusercontent.com/-3e3ttRJyOJk/AAAAAAAAAAI/AAAAAAAAAUI/i7a1mUx3_Co/photo.jpg",
        click_action: "https://storm-79ae3.firebaseapp.com"
      }
    };

    var fcmTokens = admin.firestore().collection('fcmTokens');
    var query = fcmTokens.get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          admin.messaging().sendToDevice(doc.data().token, payload);
        });
      })
      .catch(err => {
        console.log('Error getting documents', err);
      });

  });
const sendgrid = require('sendgrid');
const sendgrid_client = sendgrid("OiQqcbJWR42-SI5icgotbw");

function parseBody() {
  var helper = sendgrid.mail;
  var fromEmail = new helper.Email('phuquy.uit@gmail.com');
  var toEmail = new helper.Email('phuquy.uit@gmail.com');
  var subject = 'body.subject';
  var content = new helper.Content('text/html', 'body.content');
  var mail = new helper.Mail(fromEmail, subject, toEmail, content);
  return mail.toJSON();
}
exports.sendEmailNotification = functions.firestore
  .document('/contact/{value}').onCreate((event) => {
    const data = event.data.data();
    const payload = {
      notification: {
        title: 'Có tin nhắn mới',
        body: 'Bạn có tin nhắn từ ' + data.email,
        icon: "https://lh6.googleusercontent.com/-3e3ttRJyOJk/AAAAAAAAAAI/AAAAAAAAAUI/i7a1mUx3_Co/photo.jpg",
        click_action: "https://storm-79ae3.firebaseapp.com"
      }
    };

    var fcmTokens = admin.firestore().collection('fcmTokens');
    const request = sendgrid_client.emptyRequest({
      method: 'POST',
      path: '/v3/mail/send',
      body: parseBody()
    });

    client.API(request);
    var query = fcmTokens.get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          admin.messaging().sendToDevice(doc.data().token, payload);
        });
      })
      .catch(err => {
        console.log('Error getting documents', err);
      });

  });

const algoliasearch = require('algoliasearch');

const ALGOLIA_ID = 'UR9A905WPX';
const ALGOLIA_ADMIN_KEY = '141426c8e8ed4dd76bdbd1f61935f0f8';
const ALGOLIA_SEARCH_KEY = 'f9c5add3aa342e49488e62722c0645a4';

const ALGOLIA_INDEX_NAME = 'products';
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

exports.onProductCreated = functions.firestore.document('products/{productId}').onCreate(event => {
  // Get the note document
  const note = event.data.data();

  // Add an 'objectID' field which Algolia requires
  note.objectID = event.params.productId;

  // Write to the algolia index
  const index = client.initIndex(ALGOLIA_INDEX_NAME);
  return index.saveObject(note);
});
