(function($) {
  "use strict";

  window.onload = function() {
    $(".bt-loading").fadeOut("1500", function() {
      $('#bt_loading').css("display", "none");
    });
  };
  jQuery(window).scroll(function() {
    var height_header = $('#top').height() + $('header').height() + $('.boss_header').height() + 10;
    if ($(window).scrollTop() > height_header) {
      $('.menu').addClass('boss_scroll');
      $('.boss_header').addClass('boss_scroll');
      $('.header_category').addClass('boss_scroll');
    } else {
      $('.boss_header').removeClass('boss_scroll');
      $('.menu').removeClass('boss_scroll');
      $('.header_category').removeClass('boss_scroll');
    }

    /* Scroll Top */
    if ($(this).scrollTop() > 600) {
      $('#back_top').fadeIn();
    } else {
      $('#back_top').fadeOut();
    }
  });

})(jQuery);

function getMaxHeight($elms) {
  var maxHeight = 0;
  $($elms).each(function() {
    var height = $(this).outerHeight();
    if (height > maxHeight) {
      maxHeight = height;
    }
  });
  return maxHeight;
};
