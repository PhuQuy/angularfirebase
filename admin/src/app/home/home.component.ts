import { Component, OnInit } from '@angular/core';
import { MessagingService } from "./../services/messaging.service";
import { OrderService } from "./../services/cart/order.service";
import { ContactService } from "./../services/contact/contact.service";
import { Order } from './../models/order';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MessagingService, OrderService, ContactService]
})
export class HomeComponent implements OnInit {
  orders: any;
  contacts: any;
  constructor(private msgService: MessagingService, private orderService: OrderService, private contactService: ContactService) {
    msgService.getPermission();
    msgService.receiveMessage();

    this.orders = orderService.getOrdersList();
    this.contacts = contactService.getContacts();
  }

  ngOnInit() {
  }

}
