import {Routes, RouterModule} from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { HomeComponent } from './home/home.component';
import { Error404Component } from './error-404/error-404.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UploadFormComponent } from './uploads/upload-form/upload-form.component';
import { UpdateBlogComponent } from './admin/blogs/update-blog/update-blog.component';
import { ProductComponent } from './product/product.component';
import { UpdateProductComponent } from './product/update-product/update-product.component';
import { BlogDetailComponent } from './blog/blog-detail/blog-detail.component';
import { AuthGuard } from './core/auth.guard';
import { OrderDetailComponent } from './order-detail/order-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent, canActivate: [AuthGuard]
  },
  {
    path: 'order-detail/:id',
    component: OrderDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'blog',
    component: BlogComponent, canActivate: [AuthGuard]
  },
  {
    path: 'blog/:key',
    component: BlogDetailComponent, canActivate: [AuthGuard]
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent, canActivate: [AuthGuard]
  },
  {
    path: 'upload',
    component: UploadFormComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add-blog',
    component: UpdateBlogComponent, canActivate: [AuthGuard]
  },
  {
    path: 'product',
    component: ProductComponent, canActivate: [AuthGuard]
  },
  {
    path: 'add-product',
    component: UpdateProductComponent, canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: Error404Component
  }

];
export const routing = RouterModule.forRoot(routes);
