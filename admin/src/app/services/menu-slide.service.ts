import { Injectable } from '@angular/core';
import { Slider } from './../models/slider';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Injectable()
export class MenuSlideService {
  private basePath = '/slider';

  private itemsCollection: AngularFirestoreCollection<Slider>;

  constructor(private angularFirestore: AngularFirestore) {
    // this.productsRef = db.list('/products');

    this.itemsCollection = angularFirestore.collection<any>(this.basePath);
  }

  getSlidersList(query?) {
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Slider;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

}
