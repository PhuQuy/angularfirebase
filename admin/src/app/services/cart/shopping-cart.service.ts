import { Injectable } from '@angular/core';
import { Product } from './../../models/product';
import { CartItem } from './../../models/cart-item';
import { Observable } from "rxjs/Observable";
import { ShoppingCart } from './../../models/shopping-cart';
import { DeliveryOption } from './../../models/delivery-option';
import { Observer } from "rxjs/Observer";
import { StorageService } from "./../../services/cart/storage.service";
import { DeliveryOptionsDataService } from "./../../services/cart/delivery-options.service";

const CART_KEY = "cart";

@Injectable()
export class ShoppingCartService {
  private storage: Storage;
  private products: Product[];
  private subscriptionObservable: Observable<ShoppingCart>;
  private deliveryOptions: DeliveryOption[];
  private subscribers: Array<Observer<ShoppingCart>> = new Array<Observer<ShoppingCart>>();

  public constructor(private storageService: StorageService,
    private deliveryOptionsService: DeliveryOptionsDataService) {
    this.storage = this.storageService.get();
    this.deliveryOptionsService.all().subscribe((options) => this.deliveryOptions = options);

    this.subscriptionObservable = new Observable<ShoppingCart>((observer: Observer<ShoppingCart>) => {
      this.subscribers.push(observer);
      observer.next(this.retrieve());
      return () => {
        this.subscribers = this.subscribers.filter((obs) => obs !== observer);
      };
    });
  }

  public get(): Observable<ShoppingCart> {
    return this.subscriptionObservable;
  }

  public empty(): void {
    const newCart = new ShoppingCart();
    this.save(newCart);
    this.dispatch(newCart);
  }

  public removeItem(product: Product): void {
    const cart = this.retrieve();
    for (var i = 0; i < cart.items.length; i++) {
      if (cart.items[i].product.id === product.id) {
        cart.items.splice(i, 1);
        break;
      }
    }

    this.calculateCart(cart);
    this.save(cart);
    this.dispatch(cart);
  }

  public addItem(product: Product, quantity: number): void {
    const cart = this.retrieve();
    let item = cart.items.find(p => {
      if (!p.product) return false;
      return p.product.id === product.id;
    });
    if (item === undefined) {
      item = new CartItem();
      item.product = product;
      cart.items.push(item);
    }

    item.quantity += quantity;
    cart.items = cart.items.filter((cartItem) => cartItem.quantity > 0);
    if (cart.items.length === 0) {
      cart.deliveryOptionId = undefined;
    }

    this.calculateCart(cart);
    this.save(cart);
    this.dispatch(cart);
  }

  private retrieve(): ShoppingCart {
    const cart = new ShoppingCart();
    const storedCart = this.storage.getItem(CART_KEY);
    if (storedCart) {
      cart.updateFrom(JSON.parse(storedCart));
    }

    return cart;
  }

  private calculateCart(cart: ShoppingCart): void {
    cart.itemsTotal = cart.items
      .map((item) => item.quantity * item.product.price)
      .reduce((previous, current) => previous + current, 0);
    // cart.deliveryTotal = cart.deliveryOptionId ?
    //   this.deliveryOptions.find((x) => x.id === cart.deliveryOptionId).price :
    //   0;
    cart.deliveryTotal = 0;
    cart.grossTotal = cart.itemsTotal + cart.deliveryTotal;
  }

  private save(cart: ShoppingCart): void {
    this.storage.setItem(CART_KEY, JSON.stringify(cart));
  }

  private dispatch(cart: ShoppingCart): void {
    this.subscribers
      .forEach((sub) => {
        try {
          sub.next(cart);
        } catch (e) {
          // we want all subscribers to get the update even if one errors.
        }
      });
  }

}
