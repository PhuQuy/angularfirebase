import { Injectable } from '@angular/core';
import { Order } from './../../models/order';
import { IOrder } from './../../models/iorder';
import * as firebase from 'firebase/app';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Injectable()
export class OrderService {
  protected basePath = '/carts';
  constructor(protected angularFirestore: AngularFirestore) {

  }

  getOrdersList() {
    // return this.angularFireDatabase.list(`${this.basePath}`, ref => ref.where('userID', '==', userID)).snapshotChanges().map(changes => {
    //   return changes.map(a => {
    //     const data = a.payload.doc.data() as IOrder;
    //     data.id = a.payload.doc.id;
    //     return data;
    //   });
    // });

    return this.angularFirestore.collection<IOrder>(this.basePath, ref => ref.orderBy('createdAt', "desc")).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as IOrder;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  update(order) {
    var cityRef = this.angularFirestore.collection(this.basePath).doc(order.id);

    var setWithMerge = cityRef.set({...order,
      done: true});
  }

  get(id: string) {
    // const itemPath = `${this.basePath}/${id}`;
    // return this.angularFirestore.doc<IOrder>(itemPath).snapshotChanges();
    // return this.angularFirestore.collection<IOrder>(this.basePath).where("capital", "==", true).snapshotChanges().map(changes => {
    //   return changes.map(a => {
    //     const data = a.payload.doc.data() as IOrder;
    //     data.id = a.payload.doc.id;
    //     return data;
    //   });
    // });
    return this.angularFirestore.collection<IOrder>(this.basePath).doc(id).valueChanges();
  }

  save(order: Order) {
    const timestamp = this.timestamp;

    var data = this.mapItem(order);
    // return this.angularFireDatabase.list(`${this.basePath}`).push(order);
    return this.angularFirestore.collection<IOrder>(this.basePath).add({ ...data, updatedAt: timestamp, createdAt: timestamp });
  }

  mapItem(order: Order) {
    return {
      user: order.user,
      userID: order.userID,
      cart: order.cart,
      total: order.total,
      ward: order.ward,
      address: "phòng " + order.address + " tòa nhà " + order.ward,
      phone: order.phone,
      email: order.email,
      description: order.description
    }
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }
}
