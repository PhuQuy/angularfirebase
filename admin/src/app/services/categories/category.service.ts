import { Injectable } from '@angular/core';
import { ICategory } from './../../models/icategory';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Injectable()
export class CategoryService {
  private basePath = '/categories';
  private itemsCollection: AngularFirestoreCollection<ICategory>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<ICategory>(this.basePath);
  }

  getCategories() {
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as ICategory;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

}
