import { Injectable } from '@angular/core';
import { Upload } from './../../models/upload';
import * as firebase from 'firebase';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";

@Injectable()
export class UploadService {

  private basePath = 'uploads';
  // uploadsRef: AngularFireList<Upload>;
  // uploads: Observable<Upload[]>;
  // currentUpload: Upload;

  // constructor(private angularFirestore: AngularFirestore) { }


  // getUploads() {
  //   this.uploads = this.db.list(this.basePath).snapshotChanges().map(actions => {
  //     return actions.map(a => {
  //       const data = a.payload.val()
  //       const $key = a.payload.key;
  //       return { $key, ...data };
  //     });
  //   });
  //   return this.uploads
  // }


  deleteUpload(upload: Upload) {
    this.deleteFileStorage(upload.name);
  }

  // Executes the file uploading to firebase https://firebase.google.com/docs/storage/web/upload-files
  pushUpload(upload: Upload) {

    return Observable.create((obs: Observer<Upload>) => {
      const storageRef = firebase.storage().ref();
      const uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);
      const sub = uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          // upload in progress
          const snap = snapshot as firebase.storage.UploadTaskSnapshot;
          upload.progress = (snap.bytesTransferred / snap.totalBytes) * 100;
        },
        (error) => {
          // upload failed
          console.log(error)
        },
        () => {
          // upload success
          upload.url = uploadTask.snapshot.downloadURL;
          upload.name = upload.file.name;
          // this.saveFileData(upload);
          // return uploadTask;
          obs.next(upload);
          obs.complete();
        }
      );

    });
    //
    //
    // return new Promise((resolve, reject) => {
    //   uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
    //     (snapshot) => {
    //       // upload in progress
    //       const snap = snapshot as firebase.storage.UploadTaskSnapshot;
    //       upload.progress = (snap.bytesTransferred / snap.totalBytes) * 100;
    //     },
    //     (error) => {
    //       // upload failed
    //       console.log(error)
    //     },
    //     () => {
    //       // upload success
    //       upload.url = uploadTask.snapshot.downloadURL;
    //       upload.name = upload.file.name;
    //       // this.saveFileData(upload);
    //       // return uploadTask;
    //       resolve(upload.url)
    //     }
    //   );
    // })
  }

  // getCurrentUpload() {
  //   return this.currentUpload;
  // }
  //
  // // Writes the file details to the realtime db
  // private saveFileData(upload: Upload) {
  //   this.angularFirestore.collection(`${this.basePath}/`).add(upload);
  // }

  // Writes the file details to the realtime db
  // private deleteFileData(key: string) {
  //   return this.db.list(`${this.basePath}/`).remove(key);
  // }

  // Firebase files must have unique names in their respective storage dir
  // So the name serves as a unique key
  private deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete()
  }


}
