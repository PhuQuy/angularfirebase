import { Component, OnInit } from '@angular/core';
import { OrderService } from "./../services/cart/order.service";
import { Order } from './../models/order';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css'],
  providers: [OrderService]
})
export class OrderDetailComponent implements OnInit {
  order: any;
  orderI: any;
  id: string;
  constructor(private orderService: OrderService, private router: ActivatedRoute) {
    this.router.params.subscribe(params => {
      this.id = params['id'];
      this.orderI = orderService.get(this.id).subscribe(order => {
        this.order = order;
        this.order.id = this.id;
      });
    });
  }

  update() {
    this.orderService.update(this.order);
  }

  ngOnInit() {
    // this.orderI.subscribe(order => this.order = order);
  }
}
