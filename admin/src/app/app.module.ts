import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from 'angularfire2';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AngularFirestore, AngularFirestoreModule} from 'angularfire2/firestore';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from './core/auth.guard';
import { DeAuthGuard } from './core/de-auth.guard';
import { AngularFireAuth } from 'angularfire2/auth';

import { ProductService } from './services/product/product.service';

import {routing} from './app.routes';

import { MenuComponent } from './menu/menu.component';
import { BlogComponent } from './blog/blog.component';
import { HomeComponent } from './home/home.component';
import { Error404Component } from './error-404/error-404.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FooterComponent } from './footer/footer.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UploadFormComponent } from './uploads/upload-form/upload-form.component';
import { UpdateBlogComponent } from './admin/blogs/update-blog/update-blog.component';
import { ProductComponent } from './product/product.component';
import { UpdateProductComponent } from './product/update-product/update-product.component';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
import { BlogDetailComponent } from './blog/blog-detail/blog-detail.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';

// export const firebaseConfig = {
//   apiKey: "AIzaSyBWjC8izM73vTdSeqVlxP5ycvQHChhc2_s",
//   authDomain: "storm-79ae3.firebaseapp.com",
//   databaseURL: "https://storm-79ae3.firebaseio.com",
//   projectId: "storm-79ae3",
//   storageBucket: "storm-79ae3.appspot.com",
//   messagingSenderId: "61835654518"
// };

export const firebaseConfig = {
   apiKey: "AIzaSyCaiCbGLuYBippPPTgNwMNKPdgblV70meU",
   authDomain: "rauquagiamcan.firebaseapp.com",
   databaseURL: "https://rauquagiamcan.firebaseio.com",
   projectId: "rauquagiamcan",
   storageBucket: "rauquagiamcan.appspot.com",
   messagingSenderId: "317430466302"
 };

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    BlogComponent,
    HomeComponent,
    Error404Component,
    LoginFormComponent,
    ContactUsComponent,
    FooterComponent,
    SignUpComponent,
    UploadFormComponent,
    UpdateBlogComponent,
    ProductComponent,
    UpdateProductComponent,
    LoadingSpinnerComponent,
    BlogDetailComponent,
    OrderDetailComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    CommonModule,
    TagInputModule, BrowserAnimationsModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    AngularFirestoreModule.enablePersistence(),
    AngularFireModule.initializeApp(firebaseConfig),
    routing
  ],
  providers: [AuthGuard, AngularFireAuth, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
