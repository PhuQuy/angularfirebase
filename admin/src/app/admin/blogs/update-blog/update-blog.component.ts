import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import { UploadService } from './../../../services/upload/upload.service';
import { Upload } from './../../../models/upload';
import { BlogService } from './../../../services/blog/blog.service';
import { Blog } from './../../../models/blog';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-update-blog',
  templateUrl: './update-blog.component.html',
  styleUrls: ['./update-blog.component.css'],
  providers: [BlogService, AngularFireAuth, UploadService]
})
export class UpdateBlogComponent {
  error: any;
  currentUpload: Upload;
  showSpinner = false;

  blog: Blog = new Blog();
  items = [];

  options: any = {
    placeholderText: 'Edit Your Description Here!',
    charCounterCount: false
  }

  constructor(private afAuth: AngularFireAuth, private blogService: BlogService, private upSvc: UploadService, private router: Router) { }

  createNewBlog() {
      this.uploadBlog();
  }

  uploadBlog() {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.showSpinner = true;
        this.blog.author = auth.displayName;
        this.blog.star = 3;
        this.blog.slug = this.blog.title.split(' ').join('-');
        this.blog.authorImage = auth.photoURL;
        this.items.map(item => {
          this.blog.tags.push(item.value);
        })
        this.blogService.addBlog(this.blog).then(value => {
          this.blog = new Blog();
          this.items = [];
          this.showSpinner = false;
        });
      }
    });
  }

  detectFiles(event) {
    this.showSpinner = true;
    if (event.target.files) {
      let files = event.target.files;
      this.currentUpload = new Upload(files.item(0));
      this.upSvc.pushUpload(this.currentUpload).subscribe(uploaded => {
        this.showSpinner = false;
        this.blog.photoUrl = uploaded.url;
      })
    }
  }

  clearImage() {
    this.upSvc.deleteUpload(this.currentUpload);
    this.blog.photoUrl = null;
  }

}
