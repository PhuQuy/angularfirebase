import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [AngularFireAuth]
})
export class LoginFormComponent implements OnInit {
  error: any;
  email: any;
  password: any;

  constructor(private afAuth: AngularFireAuth, private router: Router) {
  }

  loginByGoogle() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(value => {
      console.log("nice");
    });
  }

  loginByFacebook() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(value => {
      console.log("nice");
    });
  }

  loginByEmailAndPassword(formData) {
    this.afAuth.auth.signInWithEmailAndPassword(formData.value.email, formData.value.password).then(
      (success) => {
        this.error = null;
      }).catch(
      (err) => {
        this.error = err.message;
      });
  }

  closeError() {
    this.error = null;
  }

  ngOnInit() {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.router.navigate(['']);
        console.log(auth.displayName);
      }
    });
  }

}
