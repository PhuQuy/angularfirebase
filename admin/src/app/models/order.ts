import { CartItem } from './cart-item';

export class Order {
  $key: string;
  id: string;
  user: string;
  userID: string = '';
  cart: CartItem[];
  total: number;
  address: string;
  ward: string;
  phone: string;
  email: string;
  description: string = '';
  createdAt: any;
  updatedAt: any;

  constructor() {
  }
}
