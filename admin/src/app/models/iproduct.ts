export interface Iproduct {
  id?: string;
  title?: string;
  description?: string;
  star: number;
  author: string;
  kg: boolean;
  box: boolean;
  photoUrl?: string;
  price: number;
  sale: number;
  special: boolean;
  photos: string[];
  instock: boolean;
  fullDescription: string;
  newPrice: number;
  tags: string[];
  category: string;
  provider: string;
  createdAt: any;
}
