export interface Item {
  id?: string;
  title?: string;
  description?: string;
  slug?: string;
  star: number;
  author: string;
  authorImage: string;
  photoUrl?: string;
  content: string;
  tags: string[];
  createdAt: any;
  updatedAt: any;
}
