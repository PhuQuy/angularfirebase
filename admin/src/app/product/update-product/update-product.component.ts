import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import { UploadService } from './../../services/upload/upload.service';
import { ProductService } from './../../services/product/product.service';
import { CategoryService } from './../../services/categories/category.service';
import { Upload } from './../../models/upload';
import { Product } from './../../models/product';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Http, Response } from '@angular/http';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css'],
  providers: [ProductService, UploadService, AngularFireAuth, CategoryService]
})
export class UpdateProductComponent {
  error: any;
  photos: any;
  dphotos: Array<Upload> = new Array<Upload>();
  currentUpload: Upload;
  items = [];

  product: Product = new Product();
  showSpinner = false;
  loadPhotosSpinner = false;
  submitSpinner = false;

  options: any = {
    placeholderText: 'Edit Your Description Here!',
    charCounterCount: false
  }

  categories: any = [];

  constructor(private afAuth: AngularFireAuth, private productService: ProductService, private categoryService: CategoryService, private upSvc: UploadService, private router: Router) {
    // console.log(this.product);
    this.categories = categoryService.getCategories();
  }

  createNewProduct(formData) {
    this.uploadProduct();
  }

  uploadProduct() {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.submitSpinner = true;
        this.product.author = auth.displayName;
        this.items.map(item => {
          this.product.tags.push(item.value);
        })
        this.productService.addProduct(this.product).then(value => {
          this.product = new Product();
          this.submitSpinner = false;
          this.items = [];
          this.photos = '';
        }).catch(error => {
          this.error = error;
          this.submitSpinner = false;
        });
      }
    });
  }

  selectCategory(value) {
    this.product.category = value;
  }
  getDescription() {
    console.log(this.product.description);
  }

  detectMainPhoto(event) {
    this.showSpinner = true;
    if (event.target.files) {
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.currentUpload = new Upload(files.item(i));

        this.upSvc.pushUpload(this.currentUpload).subscribe(uploaded => {
          this.showSpinner = false;
          this.product.photoUrl = uploaded.url;
          this.currentUpload.name = uploaded.name;
        })
      }
    }
  }

  detectPhotos(event) {
    if (event.target.files) {
      this.product.photos = new Array();
      this.dphotos = new Array();
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.loadPhotosSpinner = true;
        this.upSvc.pushUpload(new Upload(files.item(i))).subscribe(uploaded => {
          this.loadPhotosSpinner = false;
          this.dphotos.push(uploaded);
          this.photos += uploaded.name + '; ';
          this.product.photos.push(uploaded.url);

        })
      }
    }
  }

  clearImage() {
    this.upSvc.deleteUpload(this.currentUpload);
    this.product.photoUrl = null;
  }

  clearImages() {
    this.photos = null;
    this.product.photos = [];
    this.dphotos.map(photo => this.upSvc.deleteUpload(photo));
  }

}
