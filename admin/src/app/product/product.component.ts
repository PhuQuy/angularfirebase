import { Component, AfterViewInit } from '@angular/core';
import { ProductService } from './../services/product/product.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [AngularFireDatabase, ProductService]
})
export class ProductComponent implements AfterViewInit {
  products: any;
  showSpinner = true;

  constructor(private productService: ProductService) {
    this.products = this.productService.getProductsList();
    console.log('cun')
  }

  ngOnInit() {
    console.log('ahuhu');
    this.products.subscribe(x => {
      this.showSpinner = false;
    })
  }

  ngAfterViewInit() {
     // ...
     console.log('load nè');
   }
}
