import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [AngularFireAuth]
})
export class MenuComponent implements OnInit {
  user: any;
  name: any;

  constructor(private afAuth: AngularFireAuth) {
    this.user = this.afAuth.authState;
    this.user.subscribe(auth => {
      if (auth) {
        this.name = auth;
      }
    });
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  ngOnInit() {
  }

}
