import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UploadService } from './../uploads/shared/upload.service';
import { Upload } from './../uploads/shared/upload';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
  providers: [AngularFireAuth, UploadService, AngularFireDatabase]
})
export class SignUpComponent implements OnInit {
  error: any;
  email: any;
  name: any;
  password: any;
  photoName: string = '';
  selectedPhoto: any;
  currentUpload: Upload;
  succeedUpload: any;
  photoURL: any;

  constructor(private afAuth: AngularFireAuth, private router: Router, private upSvc: UploadService) {
  }

  signUp(formData) {
    this.name = formData.value.name;
    this.afAuth.auth.createUserWithEmailAndPassword(formData.value.email, formData.value.password).then(
      (success) => {
        this.uploadSingle();
        // this.afAuth.auth.currentUser.updateProfile({
        //   displayName: this.name,
        //   photoURL: this.photoURL
        // });
        this.error = null;
      }).catch(
      (err) => {
        this.error = err.message;
      });
  }

  ngOnInit() {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.router.navigate(['']);
      }
    });
  }

  detectFiles(event) {
    this.selectedPhoto = event.target.files;
    if (event.target.files) {
      let file = event.target.files.item(0);
      this.selectedPhoto = file;
      this.photoName = file.name;
    }
  }

  clearImage() {
    this.selectedPhoto = null;
    this.photoName = '';
  }

  uploadSingle() {
    let file = this.selectedPhoto;
    if (file) {
      this.currentUpload = new Upload(file);
      let data2 = this.upSvc.pushUpload(this.currentUpload).then(res=> {
        this.photoURL = res;
        this.afAuth.auth.currentUser.updateProfile({
          displayName: this.name,
          photoURL: this.photoURL
        });
      });
    }

  }
}
