import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { BlogService } from './blog.service';
import { Blog } from './blog';
import { CommonModule } from '@angular/common';
import { SlicePipe } from '@angular/common';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [AngularFireDatabase, BlogService]
})
export class BlogComponent implements OnInit {
  items: any;
  showSpinner = true;

  constructor(private blogService: BlogService) {
    this.items = this.blogService.getBlogs();
  }

  deleteBlog(item: Blog) {
    this.blogService.deleteBlog(item);
  }

  ngOnInit() {
    this.items.subscribe(x => {
      this.showSpinner = false;
    })
  }

}
