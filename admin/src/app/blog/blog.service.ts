import { Injectable } from '@angular/core';
import { Blog } from './blog';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';

@Injectable()
export class BlogService {
  basePath = 'blogs';
  // items: AngularFirestoreCollection<Blog>;
  itemsRef: AngularFireList<Blog>;

  constructor(private db: AngularFireDatabase) {
    this.itemsRef = this.db.list(`${this.basePath}/`, ref => ref.limitToFirst(10))
  }

  getBlogs(query?) {
    return this.itemsRef.snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }) )
    })
  }

  deleteBlog(blog: Blog) {
    return this.db.list(`${this.basePath}/`).remove(blog.$key);
  }

  addBlog(blog: Blog) {
    return this.db.list(`${this.basePath}/`).push(blog);
  }

}
