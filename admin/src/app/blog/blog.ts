export class Blog {
  $key: string;
  title:string;
  description:string;
  star: number = 0;
  author: string;
  authorImage: string;
  photoUrl: string;
  createdAt: string = new Date().toLocaleDateString('en-US');;

  constructor(title : string, desc: string) {
    this.title = title;
    this.description = desc;
  }
}
