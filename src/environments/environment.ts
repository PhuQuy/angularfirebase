// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,

  algolia: {
    appId: 'UR9A905WPX',
    apiKey: 'f9c5add3aa342e49488e62722c0645a4',
    indexName: 'products',
    urlSync: false
  }
};
