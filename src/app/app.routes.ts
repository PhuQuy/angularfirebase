import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './ui/home/home.component';
import { Error404Component } from './ui/error-404/error-404.component';
import { CartViewComponent } from './ui/cart-view/cart-view.component';
import { SearchUiComponent } from './ui/search-ui/search-ui.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: './ui/home/home.module#HomeModule'
  },
  {
    path: 'search',
    component: SearchUiComponent
  },
  {
    path: 'blog',
    loadChildren: './ui/blog-lazy/blog.module#BlogModule'
  },
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule'
  },
  {
    path: 'cart',
    component: CartViewComponent
  },
  {
    path: 'product',
    loadChildren: './ui/product-lazy/product.module#ProductModule'
  },
  {
    path: '**',
    component: Error404Component
  }

];
export const routing = RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
