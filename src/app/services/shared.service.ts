import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Product } from './../models/product';

@Injectable()
export class SharedService {
  heading: string;
  key: string;

  setHeading(newValue) {
    this.heading = newValue; //you can also do validation or other things here
  }
  getHeading() {
    return this.heading;
  }

  getKey() {
    return this.key;
  }

  setKey(newValue) {
    this.key = newValue;
  }

  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();

  // Service message commands
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  getEmitChangeSource() {
    return this.emitChangeSource;
  }

  private emitProductSource = new Subject<any>();
  // Observable string streams
  emitProductSource$ = this.emitProductSource.asObservable();

  emitProductChange(change: any) {
    this.emitProductSource.next(change);
  }

  getEmitProductChangeSource() {
    return this.emitProductSource;
  }

  private emitSearch = new Subject<any>();
  // Observable string streams
  emitSearch$ = this.emitSearch.asObservable();

  emitSearchChange(change: any) {
    this.emitSearch.next(change);
  }

}
