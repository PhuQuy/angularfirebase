import { Injectable } from '@angular/core';
import { IComment } from './../../models/icomment';
import { Comment } from './../../models/comment';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';


@Injectable()
export class CommentService {

  private basePath = '/comments';
  private itemsCollection: AngularFirestoreCollection<IComment>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<IComment>(this.basePath);
  }

  getCommentByProduct(id: string) {
    this.itemsCollection = this.angularFirestore.collection(this.basePath, ref => ref.where('productID', '==', id));
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as IComment;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  mapComment(comment: Comment) {
    const timestamp = this.timestamp;
    return {
      user: comment.user,
      userID: comment.userID,
      productID: comment.productID,
      comment: comment.comment,
      createdAt: timestamp
    }
  }

  addComment(contact: Comment) {
    var data = this.mapComment(contact);
    return this.itemsCollection.add(data);
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }
}
