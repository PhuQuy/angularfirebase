import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";
import { DeliveryOption } from './../../models/delivery-option';
import { CachcingServiceBase } from "./caching.service";

@Injectable()
export class DeliveryOptionsDataService extends CachcingServiceBase {
  private deliveryOptions: Observable<DeliveryOption[]>;

  public constructor() {
    super();
  }

  delivery: any[] = [
    {
      "id": "f488db46-9380-45e2-b15e-4ace7bdb7c89",
      "name": "Drone",
      "description": "Get your package within an hour and have it flown in by a drone!",
      "price": 19.99
    },
    {
      "id": "c7f6535c-c56b-4e57-94dc-0e95b936b00b",
      "name": "Express",
      "description": "The quickest of the normal delivery service",
      "price": 9.99
    },
    {
      "id": "caa93bc4-d69a-4788-aff6-4a6fb538ace8",
      "name": "Standard",
      "description": "Standard shipping can take up to 4 days",
      "price": 5.99
    },
    {
      "id": "c272fc43-28e4-4ffd-ae61-0cfdec565f6f",
      "name": "Pick-up",
      "description": "Pick it up tomorrow from you local post office",
      "price": 0
    }
  ]


  public all(): Observable<DeliveryOption[]> {
    return this.cache<DeliveryOption[]>(() => this.deliveryOptions,
      (val: Observable<DeliveryOption[]>) => this.deliveryOptions = val,
      () => Observable.of(this.delivery));

  }
}
