import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable()
export class SeoService {
  constructor(private meta: Meta, private title: Title) { }
  generateTags(config) {
    // default values
    config = {
      title: 'Vege Shop',
      description: 'Hoa quả ký túc xá khu B',
      image: 'https://firebasestorage.googleapis.com/v0/b/rauquagiamcan.appspot.com/o/uploads%2Fhoprauqua1.webp?alt=media&token=407af072-925e-4d3c-9ae7-b375cc33a4e0',
      slug: '',
      ...config
    }
    this.meta.updateTag({ name: 'slug', content: config.slug });
    this.meta.updateTag({ name: 'image', content: config.image });
    this.meta.updateTag({ name: 'description', content: config.description });
    this.meta.updateTag({ name: 'title', content: config.title });
    this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
    this.meta.updateTag({ name: 'twitter:site', content: '@hoaquaktx' });
    this.meta.updateTag({ name: 'twitter:title', content: config.title })
    this.meta.updateTag({ name: 'twitter:description', content: config.description });
    this.meta.updateTag({ name: 'twitter:image', content: config.image });

    this.meta.updateTag({ property: 'og:type', content: 'website' });
    this.meta.updateTag({ property: 'og:site_name', content: 'VegeShop | Hoa quả ký túc xá' });
    this.meta.updateTag({ property: 'og:title', content: config.title });
    this.meta.updateTag({ property: 'og:description', content: config.description });
    this.meta.updateTag({ property: 'og:image', content: config.image });
    this.meta.updateTag({ property: 'og:url', content: `https://rauquasach.ga/${config.slug}` });
  }

  genrateTitle(tit: string) {
    this.title.setTitle(tit);
  }
}
