import { Injectable } from '@angular/core';
import { IContact } from './../../models/icontact';
import { Contact } from './../../models/contact';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()
export class ContactService {

  private basePath = '/contact';
  private itemsCollection: AngularFirestoreCollection<IContact>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<IContact>(this.basePath);
  }

  getCategories() {
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as IContact;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  mapContact(contact: Contact) {
    const timestamp = this.timestamp;
    return {
      name: contact.name,
      email: contact.email,
      message: contact.message,
      createdAt: timestamp
    }
  }

  addContact(contact: Contact) {
    var data = this.mapContact(contact);
    return this.itemsCollection.add(data);
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

}
