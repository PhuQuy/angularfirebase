import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from 'angularfire2';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';

import 'hammerjs';

import { routing } from './app.routes';

import { ProductInfoModule } from './ui/product-info/product-info.module';

import { SharedService } from './services/shared.service';
import { CachcingServiceBase } from './services/cart/caching.service';
import { DeliveryOptionsDataService } from './services/cart/delivery-options.service';
import { ShoppingCartService } from './services/cart/shopping-cart.service';
import { LocalStorageServie, StorageService } from './services/cart/storage.service';
import { SeoService } from './services/seo/seo.service';

import { MenuComponent } from './ui/menu/menu.component';
import { Error404Component } from './ui/error-404/error-404.component';
import { FooterComponent } from './ui/footer/footer.component';
import { MenuMobileComponent } from './ui/menu/menu-mobile/menu-mobile.component';
import { CartComponent } from './ui/cart/cart.component';
import { CartViewComponent } from './ui/cart-view/cart-view.component';
import { SearchUiComponent } from './ui/search-ui/search-ui.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCaiCbGLuYBippPPTgNwMNKPdgblV70meU",
  authDomain: "rauquagiamcan.firebaseapp.com",
  databaseURL: "https://rauquagiamcan.firebaseio.com",
  projectId: "rauquagiamcan",
  storageBucket: "rauquagiamcan.appspot.com",
  messagingSenderId: "317430466302"
};

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    Error404Component,
    FooterComponent,
    MenuMobileComponent,
    CartComponent,
    CartViewComponent,
    SearchUiComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    CommonModule,
    ProductInfoModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    routing
  ],
  providers: [SharedService, AngularFireAuth, SeoService,
    DeliveryOptionsDataService,
    LocalStorageServie,
    { provide: StorageService, useClass: LocalStorageServie },
    {
      deps: [StorageService, DeliveryOptionsDataService],
      provide: ShoppingCartService,
      useClass: ShoppingCartService
    }
  ],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
