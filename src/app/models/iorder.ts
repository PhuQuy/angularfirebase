import { CartItem } from './cart-item';

export interface IOrder {
  id?: string;
  user?: string;
  userID?: string;
  cart?: CartItem[];
  total: number;
  address: string;
  ward: string;
  phone: string;
  email?: string;
  description: string;
  createdAt: any;
  updatedAt: any;
}
