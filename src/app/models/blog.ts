export class Blog {
  $key: string;
  id: string;
  slug: string;
  title:string = '';
  description:string = '';
  star: number = 0;
  author: string;
  authorImage: string = null;
  photoUrl: string = null;
  content: string = '';
  tags: string[] = [];
  createdAt: any;

  constructor() {}

}
