export class Comment {
  id?: string;
  user?: string;
  userID?: string = null;
  productID: string;
  comment: string = '';
  createdAt: any;

  constructor() {
  }
}
