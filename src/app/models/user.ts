export class User {
  $key: string;
  firstName: string;
  lastName: string;
  email: string;
  emailVerified: boolean;
  isAnonymous: boolean;
  phoneNumber: string;
  uid: string;

  constructor() {}

}
