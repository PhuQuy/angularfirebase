export interface IComment {
  id?: string;
  user?: string;
  userID?: string;
  productID: string;
  comment: string;
  createdAt: any;
}
