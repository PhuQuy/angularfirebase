export class Product {
  $key: string;
  id: string;
  title:string;
  description:string = '';
  star: number = 0;
  author: string;
  kg: boolean = false;
  box: boolean = true;
  sale: number = 0;
  special: boolean = false;
  photoUrl: string = null;
  photos: string[];
  price: number;
  instock: boolean = true;
  fullDescription: string = '';
  provider: string = '';
  newPrice: number = null;

  constructor() {
  }
}
