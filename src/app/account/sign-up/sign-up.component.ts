import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Upload } from './../../models/upload';
import { Observable } from 'rxjs/Observable';
import { AccountComponent } from './../account.component';
import { User } from './../../models/user';
import { SharedService } from './../../services/shared.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent extends AccountComponent {
  password: any;
  photoName: string = '';
  selectedPhoto: any;
  currentUpload: Upload;
  succeedUpload: any;
  photoURL: any;

  authState: any = null;
  user: User = new User();

  ngOnInit() {
    this.sharedService.setHeading('Đăng ký');
  }

  signUp() {
    this.showSpinner = true;
    return this.afAuth.auth.createUserWithEmailAndPassword(this.user.email, this.password)
      .then((user) => {
        this.showSpinner = false;
        this.authState = user;
        this.uploadSingle();
        this.updateUserData();
        user.sendEmailVerification().then(function() {
          // Email sent.
        }).catch(function(error) {
          // An error happened.
          this.showSpinner = false;
        });
      })
      .catch(error => {
        this.error = error;
        this.showSpinner = false;
      });
  }

  //// Helpers ////
  // Returns true if user is logged in
  get authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  private updateUserData(): void {
    // Writes user name and email to realtime db
    // useful if your app displays information about users or for admin features

    const path = `users/${this.currentUserId}`; // Endpoint on firebase
    const userRef: any = this.db.object(path);
    userRef.update(this.user)
      .catch(error => console.log(error));
  }

  detectFiles(event) {
    this.selectedPhoto = event.target.files;
    if (event.target.files) {
      let file = event.target.files.item(0);
      this.selectedPhoto = file;
      this.photoName = file.name;
    }
  }

  clearImage() {
    this.selectedPhoto = null;
    this.photoName = '';
  }

  uploadSingle() {
    let file = this.selectedPhoto;
    if (file) {
      this.currentUpload = new Upload(file);
      let data2 = this.upSvc.pushUpload(this.currentUpload).then(res => {
        this.photoURL = res;
        this.afAuth.auth.currentUser.updateProfile({
          displayName: this.user.firstName + ' ' + this.user.lastName,
          photoURL: this.photoURL
        });
      });
    } else {
      this.afAuth.auth.currentUser.updateProfile({
        displayName: this.user.firstName + ' ' + this.user.lastName,
        photoURL: this.photoURL
      });
    }

  }
}
