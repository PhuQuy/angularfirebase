import { Component } from '@angular/core';
import * as firebase from 'firebase/app';
import { AccountComponent } from './../account.component';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent extends AccountComponent {
  error: any;
  email: any;
  password: any;

  loginByGoogle() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(value => {
      this.router.navigate(['/']);
    });
  }

  loginByFacebook() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(value => {
      this.router.navigate(['/']);
    });
  }

  loginByEmailAndPassword(formData) {
    this.showSpinner = true;
    this.afAuth.auth.signInWithEmailAndPassword(formData.value.email, formData.value.password).then(
      (success) => {
        this.error = null;
        this.showSpinner = false;
        this.router.navigate(['/']);
      }).catch(
      (err) => {
        this.showSpinner = false;
        this.error = err.message;
      });
  }

  ngOnInit() {
    this.sharedService.setHeading('Đăng nhập');
  }
}
