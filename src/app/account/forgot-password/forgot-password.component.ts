import { Component } from '@angular/core';
import { AccountComponent } from './../account.component';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent extends AccountComponent {

  password: string = '';
  email: string = '';

  resetPassword() {
    const fbAuth = firebase.auth();
    this.showSpinner = true;
    this.closeAlert();
    return fbAuth.sendPasswordResetEmail(this.email)
      .then(() => {
        this.showSpinner = false;
        this.success = 'Email đã được gửi';
      }).catch((error) => {
        this.error = error;
        this.showSpinner = false;
      })
  }

  ngOnInit() {
    this.sharedService.setHeading('Quên mật khẩu');
  }
}
