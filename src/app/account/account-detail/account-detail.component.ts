import { Component, OnInit } from '@angular/core';
import { AccountComponent } from './../account.component';
import { User } from './../../models/user';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.css']
})
export class AccountDetailComponent extends AccountComponent {
  user: User = new User();
  userRef: any;
  updateUser() {
    this.showSpinner = true;
    this.userRef.update(this.user).then(respone => this.showSpinner = false)
      .catch(error => console.log(error));
  }

  ngOnInit() {
    if (this.auth) {
      const path = `users/${this.auth.uid}`; // Endpoint on firebase
      this.userRef = this.db.object(path);
      this.userRef.valueChanges().subscribe(user => {
        if (user) {
          this.user = <User>user;
        }
      })
    }
  }

}
