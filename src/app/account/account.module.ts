import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { DeAuthGuard } from './../core/de-auth.guard';
import { AuthGuard } from './../core/auth.guard';
import { AccountComponent } from './account.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      { path: '', component: AccountDetailComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginFormComponent, canActivate: [DeAuthGuard] },
      { path: 'register', component: SignUpComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent }
    ]
  },
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccountComponent, AccountDetailComponent, LoginFormComponent, SignUpComponent, ForgotPasswordComponent],
  providers: [AngularFireAuth, DeAuthGuard, AuthGuard]
})
export class AccountModule { }
