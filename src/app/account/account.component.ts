import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { UploadService } from './../services/upload/upload.service';
import { SharedService } from './../services/shared.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [AngularFireDatabase, UploadService]
})
export class AccountComponent implements OnInit {
  public error: any;
  public success: any;
  public name: string;
  public auth: any = null;
  public showSpinner = false;

  constructor(public afAuth: AngularFireAuth, protected db: AngularFireDatabase,public router: Router,
    public sharedService: SharedService, protected upSvc: UploadService) {
    this.afAuth.authState.subscribe((auth) => {
      this.auth = auth;
    });
    sharedService.setHeading('');
  }

  heading() {
    return this.sharedService.getHeading();
  }

  public closeAlert() {
    this.success = null;
    this.error = null;
  }

  logout() {
    this.afAuth.auth.signOut();
    location.reload();
  }

  ngOnInit() {
  }
}
