import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CartItem } from './../../models/cart-item';
import { Product } from './../../models/product';
import { ShoppingCart } from './../../models/shopping-cart';
import { ShoppingCartService } from './../../services/cart/shopping-cart.service';
import { OrderService } from './../../services/cart/order.service';
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import { Order } from './../../models/order';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.css'],
  providers: [OrderService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartViewComponent implements OnInit {
  cart: Observable<ShoppingCart>;
  cartItems: CartItem[];
  cartSubscription: any;
  itemCount: number;
  email: string;
  orderItem: Order = new Order();
  total: number;
  showSpinner = false;
  @ViewChild('modalButton') modalButton: ElementRef;

  constructor(private shoppingCartService: ShoppingCartService, private orderService: OrderService) { }

  public ngOnInit(): void {
    this.cart = this.shoppingCartService.get();
    this.cartSubscription = this.cart.subscribe((cart) => {
      this.cartItems = cart.items;
      this.total = cart.grossTotal;
      this.itemCount = cart.items.map((x) => x.quantity).reduce((p, n) => p + n, 0);
    });
  }

  public ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }

  change(product: Product, quantity: number) {
    this.shoppingCartService.setQuantity(product, quantity);
  }

  deleteProductFromCart(product: Product): void {
    this.shoppingCartService.removeItem(product);
  }

  public addProductToCart(product: Product, quantity: number): void {
    this.shoppingCartService.addItem(product, quantity);
  }

  // public removeProductFromCart(product: Product, quantity: number): void {
  //   this.shoppingCartService.addItem(product, -quantity);
  // }


  order() {
    this.orderItem.total = this.total;
    this.orderItem.cart = this.cartItems;
    this.showSpinner = true;
    this.orderService.save(this.orderItem).then(value => {
      this.orderItem = new Order();
      this.showSpinner = false;
      this.modalButton.nativeElement.click();
      this.shoppingCartService.empty();
    });
  }
}
