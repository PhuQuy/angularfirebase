import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SmaillProductComponent } from './smaill-product.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [SmaillProductComponent],
  exports: [SmaillProductComponent]
})
export class SmaillProductModule { }
