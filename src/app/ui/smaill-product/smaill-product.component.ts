import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-smaill-product',
  templateUrl: './smaill-product.component.html',
  styleUrls: ['./smaill-product.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SmaillProductComponent {
  @Input() items: any;
  @Input() title: string;
}
