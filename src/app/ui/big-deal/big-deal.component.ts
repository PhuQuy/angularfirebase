import { Component, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-big-deal',
  templateUrl: './big-deal.component.html',
  styleUrls: ['./big-deal.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BigDealComponent {
  @Input() today_deal: any;
}
