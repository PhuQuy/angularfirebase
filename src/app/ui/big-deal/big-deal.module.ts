import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCartButtonModule } from './../add-cart-button/add-cart-button.module';
import { Routes, RouterModule } from '@angular/router';
import { BigDealComponent } from './big-deal.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AddCartButtonModule
  ],
  declarations: [BigDealComponent],
  exports: [BigDealComponent]
})
export class BigDealModule { }
