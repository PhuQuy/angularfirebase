import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxCarouselModule } from 'ngx-carousel';
import { ProductImageSliderComponent } from './product-image-slider.component';
import { ImageModule } from './../image/image.module';

@NgModule({
  imports: [
    CommonModule,
    ImageModule,
    NgxCarouselModule
  ],
  declarations: [ProductImageSliderComponent],
  exports: [ProductImageSliderComponent]
})
export class ProductImageSliderModule { }
