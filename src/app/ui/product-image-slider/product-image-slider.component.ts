import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';

@Component({
  selector: 'app-product-image-slider',
  templateUrl: './product-image-slider.component.html',
  styleUrls: ['./product-image-slider.component.css']
})
export class ProductImageSliderComponent implements OnInit {
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  @Input() items: Array<string>;
  @Output() loadImage = new EventEmitter<string>();

  constructor() {
    // this.carouselTileItems = items2;
    // this.carouselTileItems = this.items;
    // console.log(this.items);
    this.carouselTile = {
      grid: { xs: 3, sm: 2, md: 3, lg: 4, all: 0 },
      slide: 2,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: false
      },
      load: 2,
      touch: true,
      easing: 'ease'
    }
  }

  selectImage(img) {
    this.loadImage.emit(img);
  }

  loading(image) {
    image.isloading = true;
  }

  ngOnInit() {
    this.carouselTileItems = this.items;
  }

}
