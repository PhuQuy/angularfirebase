import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChange } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { BlogService } from './../../../services/blog/blog.service';
import { Blog } from './../../../models/blog';
import { ActivatedRoute } from "@angular/router";
import { BlogComponent } from './../blog/blog.component';
import { SharedService } from './../../../services/shared.service';
import { Comment } from './../../../models/comment';
import { CommentService } from './../../../services/comment/comment.service';
import { PaginationInstance } from 'ngx-pagination';
import { AngularFireAuth } from 'angularfire2/auth';

declare var $: any;
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css'],
  providers: [BlogService, CommentService]
})
export class BlogDetailComponent {
  blog: any;
  isNotFound = false;
  isgetByKey = false;
  relatedBlogs: any;
  comments: any = [];
  comment: Comment = new Comment();
  id: string;
  showSpinner = true;

  config: PaginationInstance = {
    id: 'comment',
    itemsPerPage: 6,
    currentPage: 1
  };

  constructor(private sharedService: SharedService, private afAuth: AngularFireAuth, private commentService: CommentService, private router: ActivatedRoute, private blogService: BlogService) {
    this.relatedBlogs = this.blogService.getBlogs();
    this.moveTop(0);
    this.router.params.subscribe(params => {
      sharedService.setHeading(params['key']);
      if (sharedService.getKey()) {
        this.blogService.get(sharedService.getKey()).subscribe(blog => {
          this.showSpinner = false;
          if (!blog) {
            this.isNotFound = true;
            return;
          }
          this.blog = blog;
        });
        this.id = sharedService.getKey();
        commentService.getCommentByProduct(sharedService.getKey()).subscribe(comments => this.comments = comments);

        return;
      }

      blogService.getBySlug(params['key']).subscribe(blog => {
        this.showSpinner = false;
        if (!blog || blog.length == 0) {
          this.isNotFound = true;
          return;
        }
        this.blog = blog[0];
        this.id = this.blog.id;
        commentService.getCommentByProduct(this.blog.id).subscribe(comments => this.comments = comments);
        this.showSpinner = false;
      })
      this.isgetByKey = false;
    });
  }

  ngOnInit() {

  }

  createComment() {
    this.comment.productID = this.id;
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.comment.userID
      }
      this.commentService.addComment(this.comment).then(response => {
        this.comment = new Comment();
      });
    });
  }

  updateBlog(blog: Blog) {
    this.moveTop(140);
  }

  moveTop(value: number) {
    $('body,html').animate({
      scrollTop: value
    }, 800, 'swing');
  }

  getTitle(): string {
    return 'Related Articles: (4)';
  }

}
