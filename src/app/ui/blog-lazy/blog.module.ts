import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { PaginationModule } from './../pagination/pagination.module';
import { BlogSliderModule } from './../blog-slider/blog-slider.module';

import { BlogComponent } from './blog/blog.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';

const routes: Routes = [
  {
    path: '', component: BlogComponent,
      children: [
        { path: '', component: BlogListComponent },
        { path: ':key', component: BlogDetailComponent }
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    Ng2OrderModule,

    PaginationModule,
    BlogSliderModule,

    RouterModule.forChild(routes)
  ],
  declarations: [BlogComponent, BlogListComponent, BlogDetailComponent]
})
export class BlogModule { }
