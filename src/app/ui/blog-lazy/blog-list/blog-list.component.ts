import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { BlogComponent } from './../blog/blog.component';
import { Blog } from './../../../models/blog';
import { PaginationInstance } from 'ngx-pagination';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent extends BlogComponent {
  top = true;
  bottom = false;
  term: string;
  key: string = 'title'; //set default
  filter: any;
  showSpinner = true;

  public config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 6,
    currentPage: 1
  };

  ngOnInit() {
    this.items = this.blogService.getBlogs();
    this.items.subscribe(items => {
      this.showSpinner = false;
    })
    this.sharedService.changeEmitted$.subscribe(
      text => {
        this.filter = text;
        // this.items = this.blogService.search(text);
      });

    // this.sharedService.emitSearch$.subscribe(text => {
    //   this.filterx = text;
    // })
  }

  readMore(blog: Blog) {
    this.sharedService.setKey(blog.id);
    // this.sharedService.emitChange(blog.id);
    this.router.navigate(['/blog', blog.slug]);
    // this.activateEvents.emit(blog);
  }

  getPage(event: any) {
    console.log('page changed');
  }
}
