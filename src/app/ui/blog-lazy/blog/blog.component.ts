import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { BlogService } from './../../../services/blog/blog.service';
import { Blog } from './../../../models/blog';
import { Item } from './../../../models/item';
import { CommonModule } from '@angular/common';
import { Router } from "@angular/router";
import { SharedService } from './../../../services/shared.service';
import { Subject } from 'rxjs/Subject'
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [AngularFireDatabase, BlogService]
})
export class BlogComponent implements OnInit {
  public items: any;
  startAt = new Subject();
  endAt = new Subject();
  key: string;
  term: string = '';

  startobs = this.startAt.asObservable();
  endobs = this.endAt.asObservable();

  public blog: any;

  constructor(protected blogService: BlogService, protected router: Router,
    protected sharedService: SharedService, private afs: AngularFirestore) {
    this.items = this.blogService.getBlogs();
    sharedService.setHeading('');

  }

  trackById(index, item) {
    return index;
  }

  filterSearch(value) {
    console.log(value);
    this.sharedService.emitSearchChange(value);
  }

  firequery(start, end) {
    return this.afs.collection('blogs', ref => ref.limit(4).orderBy('title').startAt(start).endAt(end)).valueChanges();
  }

  deleteBlog(item: Blog) {
    this.blogService.deleteBlog(item);
  }

  readMore(blog: Blog) {
    this.sharedService.setKey(blog.id);
    // this.key = blog.id;
    // this.sharedService.emitChange(blog.id);
    this.router.navigate(['/blog', blog.slug]);
  }

  heading() {
    return this.sharedService.getHeading();
  }

  search(value: string) {
    //
    // this.startAt.next(value);
    // this.endAt.next(value + "\uf8ff");
    // this.items = this.blogService.search(value);
    this.router.navigate(['/blog']);

    this.sharedService.emitChange(value);
    //
    // console.log(this.endAt.value);
    // this.items = this.firequery(this.startAt, this.endAt);
    //
    // this.items.subscribe(items => {
    //   console.log(items);
    // })
  }

  ngOnInit() {
    // this.blogService.addItem(this.item);
    // Observable.combineLatest(this.startobs, this.endobs).subscribe((value) => {
    //   // this.items = this.blogService.search(value[0], value[1]);
    //   this.firequery(value[0], value[1]).subscribe((clubs) => {
    //     console.log(clubs);
    //     // this.items = clubs;
    //   })
    // })

  }

}
