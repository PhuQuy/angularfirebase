import { Component } from '@angular/core';
import { BlogService } from './../../services/blog/blog.service';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-blog-review',
  templateUrl: './blog-review.component.html',
  styleUrls: ['./blog-review.component.css'],
  providers: [BlogService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogReviewComponent {
  blogs: any;

  constructor(private blogService: BlogService) {
    this.blogs = this.blogService.getBlogs();
  }

}
