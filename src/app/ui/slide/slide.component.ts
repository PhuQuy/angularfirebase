import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideComponent implements OnInit {
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  isloading = true;

  constructor() {
  }

  loading() {
    this.isloading = false;
  }

  trackById(index, item) {
    return index;
  }

  ngOnInit() {
    this.carouselTileItems = ["assets/images/slide/slide1.jpg", "assets/images/slide/slide2.jpg", "assets/images/slide/slide1.jpg", "assets/images/slide/slide2.jpg"];

    this.carouselTile = {
      grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
      slide: 1,
      speed: 400,
      animation: 'lazy',
      interval: 4000,
      point: {
        visible: true,
        pointStyles: `
          .ngxcarouselPoint {
            list-style-type: none;
            text-align: center;
            position: absolute;
            padding: 12px;
            width: 100%;
            bottom: 20px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            box-sizing: border-box;
          }
          .ngxcarouselPoint li {
            display: inline-block;
            border-radius: 50%;
            background: rgba(0, 0, 0, 0.55);
            padding: 4px;
            margin: 0 4px;
            transition-timing-function: cubic-bezier(0.17, 0.67, 0.83, 0.67);
            transition: 0.4s;
          }
          .ngxcarouselPoint li.active {
            background: #6b6b6b;
            transform: scale(1.8);
          }
        `
      },
      load: 2,
      loop: true,
      touch: false,
      easing: 'easeInOutBack'
    }
  }

}
