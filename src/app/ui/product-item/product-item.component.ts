import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Product } from './../../models/product';
import { ShoppingCartService } from './../../services/cart/shopping-cart.service';
import { SharedService } from './../../services/shared.service';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css'],
})
export class ProductItemComponent {
  @Input() product: any;
  @Output() details = new EventEmitter<Product>();
  isloading = true;
  public constructor(private shoppingCartService: ShoppingCartService, protected sharedService: SharedService) {

  }

  loading() {
    this.isloading = false;
  }

  clickOnProduct(product: Product) {
    this.details.emit(product);
  }

  addCart(product: Product) {
    this.shoppingCartService.addItem(product, 1);
  }

  showModal(prod: Product) {
    // this.sharedService.setProduct(prod);
    this.sharedService.emitProductChange(prod);
  }

  public productInCart(product: Product): boolean {
    return Observable.create((obs: Observer<boolean>) => {
      const sub = this.shoppingCartService
        .get()
        .subscribe(cart => {
          obs.next(cart.items.some(i => i.product.id === product.id));
          obs.complete();
        });
      sub.unsubscribe();
    });
  }

  public ngOnInit(): void {

  }

}
