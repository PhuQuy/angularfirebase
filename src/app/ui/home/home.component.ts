import { Component } from '@angular/core';
import { SeoService } from './../../services/seo/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  constructor(private seo: SeoService) { }
  ngOnInit() {
    this.seo.generateTags({
      title: 'Hoa quả sạch',
      description: 'Hoa quả sạch ký túc xá',
      image: 'https://firebasestorage.googleapis.com/v0/b/rauquagiamcan.appspot.com/o/uploads%2Fhoprauqua1.webp?alt=media&token=407af072-925e-4d3c-9ae7-b375cc33a4e0',
      slug: ''
    });

    this.seo.genrateTitle('Hoa quả sạch');
  }
}
