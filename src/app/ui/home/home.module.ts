import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LeftNavComponent } from './../left-nav/left-nav.component';
import { SlideComponent } from './../slide/slide.component';
import { BlogReviewComponent } from './../blog-review/blog-review.component';
import { OverViewComponent } from './../over-view/over-view.component';
import { HomeComponent } from './home.component';
import { ProductSliderComponent } from './../product-slider/product-slider.component';

import { SmaillProductModule } from './../smaill-product/smaill-product.module';
import { NgxCarouselModule } from 'ngx-carousel';
import { ProductItemModule } from './../product-item/product-item.module';
import { BigDealModule } from './../big-deal/big-deal.module';
import { LoadingSpinnerModule } from './../loading-spinner/loading-spinner.module';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    SmaillProductModule,
    NgxCarouselModule,
    ProductItemModule,
    BigDealModule,
    LoadingSpinnerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeComponent, OverViewComponent, BlogReviewComponent, SlideComponent, LeftNavComponent, ProductSliderComponent]
})
export class HomeModule { }
