import { Component } from '@angular/core';
import { Product } from './../../models/product';
import { SharedService } from './../../services/shared.service';

declare var $: any;
@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent {
  $element;
  product: Product;
  public carouselTileItems: Array<any>;

  constructor(protected sharedService: SharedService) {
    sharedService.emitProductSource$.subscribe(
      text => {
        this.product = text;
        this.product.photos.unshift(this.product.photoUrl);
      });
  }

  loadImage(event) {
    this.product.photoUrl = event;
  }

  showModal() {
    this.$element = $('#myModal');
    // this.product = product;
    this.$element.modal('show');
    // console.log(this.product);
  }
}
