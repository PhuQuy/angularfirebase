import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductImageSliderModule } from './../product-image-slider/product-image-slider.module';
import { ProductInfoComponent } from './product-info.component';
import { AddCartButtonModule } from './../add-cart-button/add-cart-button.module';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ProductImageSliderModule,
    AddCartButtonModule
  ],
  declarations: [ProductInfoComponent],
  exports: [ProductInfoComponent]
})
export class ProductInfoModule { }
