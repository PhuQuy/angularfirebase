import { Component, AfterViewInit } from '@angular/core';
import { CategoryService } from './../../services/categories/category.service';

declare var $: any;
@Component({
  selector: 'app-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css'],
  providers: [CategoryService]
})
export class LeftNavComponent implements AfterViewInit {
  categories: any;
  cachua: string = "MadKbGcYy0Dx86zSASfh";
  bapcai: string = "qJJem5ZO9U2v6g1Jr4Cs";
  dau: string = "wgpsqhQTDwZgoroDU62f";
  khac: string = "FZSGNaEi6RxRZCxExt6i"
  destroyMan: any;
  constructor(private categoryService: CategoryService) {
    this.destroyMan = this.categoryService.getCategories().subscribe(categories => {
      this.categories = categories;
    })
  }

  ngOnDestroy() {
    if(this.destroyMan) {
      this.destroyMan.unsubscribe();
    }
  }

  ngAfterViewInit() {
    loadtopmenu();
    $("#boss-menu-category .boss_heading").on('click', function() {
      $('#boss-menu-category').toggleClass('opencate');
      loadtopmenu();
    });

    function loadtopmenu() {
      var menuheight = $('#boss-menu-category .box-content').outerHeight();
      var topcate = $('#boss-menu-category').offset().top;
      $('#boss-menu-category .boss-menu-cate .nav_title').each(function(index, element) {
        var liheight = $(this).outerHeight();
        var subheight = $(this).next('.nav_submenu').outerHeight();
        var topheight = $(this).offset().top - topcate - 55;
      });
    }
    $('#boss-menu-category .b_menucategory_hidde,#boss-menu-category  .menu_loadmore_hidden').hide();
    $('#boss-menu-category .menu_loadmore').on('click', function() {
      $('#boss-menu-category .b_menucategory_hidde').slideToggle("normal", function() {
        $('#boss-menu-category .menu_loadmore').hide();
        $('#boss-menu-category .menu_loadmore_hidden').show();
      });
    });
    $('#boss-menu-category .menu_loadmore_hidden').on('click', function() {
      $('#boss-menu-category .b_menucategory_hidde').slideToggle("normal", function() {
        $('#boss-menu-category .menu_loadmore').show();
        $('#boss-menu-category .menu_loadmore_hidden').hide();
      });
    });
  }

}
