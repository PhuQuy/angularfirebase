import { Component } from '@angular/core';
import { ContactService } from './../../services/contact/contact.service';
import { Contact } from './../../models/contact';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers: [ContactService]
})
export class FooterComponent {
  contact: Contact = new Contact();
  emailContact = new Contact();
  showSpinner = false;
  constructor(private contactservice: ContactService) {}

  submitContact() {
    this.showSpinner = true;
    this.contactservice.addContact(this.contact).then(respone => {
      this.contact = new Contact();
      this.showSpinner = false;
    });
  }

  submitEmail() {
    this.emailContact.message="Muốn đăng tin tức";
    this.emailContact.name = "Đăng tin";
    this.contactservice.addContact(this.emailContact).then(respone => {
      this.emailContact = new Contact();
    });
  }
}
