import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { ProductInfoComponent } from '../product-info/product-info.component';
import { Router } from "@angular/router";

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.css']
})
export class ProductSliderComponent implements OnInit {

  @ViewChild('prevButton') prevButton: ElementRef;
  @ViewChild('nextButton') nextButton: ElementRef;

  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  @Input() title: string = 'No title';
  @Input() class: string = '';
  @Input() products: any;
  @Input() minSize: number;
  @Input() maxSize: number;
  @Input() logo: string;

  @Input() quickView: ProductInfoComponent;

  showSpinner = true;
  isSpecial = false;

  currentProduct: any;
  today_deal: any;

  destroyMan: any;

  constructor(private router: Router) {

  }

  ngOnInit() {
    if (this.class.length == 0) {
      this.isSpecial = true;
    }

    this.destroyMan = this.products.subscribe(products => {
      if (products.length > 0) {
        this.today_deal = products[0];
      }
      this.showSpinner = false;
      this.carouselTileItems = products;
    })

    this.carouselTile = {
      grid: { xs: this.minSize, sm: 2, md: this.maxSize, lg: this.maxSize, all: 0 },
      slide: 2,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: !this.isSpecial
      },
      load: 2,
      touch: true,
      easing: 'ease'
    }
  }

  ngOnDestroy() {
    this.destroyMan.unsubscribe();
  }

  prev() {
    this.prevButton.nativeElement.click();
  }

  next() {
    this.nextButton.nativeElement.click();
  }

  // quickViewShow = (product) => {
  //   this.quickView.showModal(product);
  // };

  details(product) {
    this.router.navigate(['/product', product.id]);
  }

  public carouselTileLoad(evt: any) { }

}
