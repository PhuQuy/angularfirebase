import { Component, AfterViewInit, Input } from '@angular/core';
import { ProductService } from './../../services/product/product.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { ProductInfoComponent } from '../product-info/product-info.component';

@Component({
  selector: 'app-over-view',
  templateUrl: './over-view.component.html',
  styleUrls: ['./over-view.component.css'],
  providers: [AngularFireDatabase, ProductService]
})
export class OverViewComponent implements AfterViewInit {
  //Today's Deals
  today_deals_title: string = "Today's Deals";
  today_deals_class: string = '';

  // Organic
  organic_goods_title: string = 'Sản phẩm mới';
  organic_goods_class: string = 'vaccines';
  organic_goods_logo: string = 'fa-thumbs-up';

  // Organic & Natural
  organic_n_natural_title: string = 'Đặc biệt';
  organic_n_natural_class: string = 'cosmetics';
  organic_n_natural_logo: string = 'fa-bar-chart';


  // Organic Vegetables
  organic_vegetables_title: string = 'Giảm giá';
  organic_vegetables_class: string = 'vitamins';
  organic_vegetables_logo: string = 'fa-pagelines';

  title_best_product: string = 'Sản phẩm tốt nhất';

  organic_goods: any;
  special_vegestable: any;
  on_sale_products: any;
  small_items: any;


  @Input() quickView: ProductInfoComponent;

  constructor(private productService: ProductService) {
    this.organic_goods = this.productService.getProductsList();
    this.special_vegestable = this.productService.getSpecial();
    this.on_sale_products = this.productService.getOnSale();
    this.small_items = this.productService.getLimits(5);
}

ngAfterViewInit() {

}

}
