import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Router } from "@angular/router";
import { SharedService } from './../../services/shared.service';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit{
  user: any;
  key: string;
  destroyMan: any;

  constructor(private afAuth: AngularFireAuth, private router: Router, private sharedService: SharedService) {
    this.destroyMan = this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.user = auth;
        console.log(auth);
      }
    });
  }

  ngOnDestroy() {
    if(this.destroyMan) {
      this.destroyMan.unsubscribe();
    }
  }

  logout() {
    this.afAuth.auth.signOut();
    location.reload();
  }

  search() {
    this.router.navigate(['/product']);
      console.log('enter');
     setTimeout(()=>{ this.sharedService.emitSearchChange(this.key); }, 1000)
     // this.sharedService.emitSearchChange();
  }

  liveSearch() {

    this.sharedService.emitSearchChange(this.key);
  }

  // Sends email allowing user to reset password
  resetPassword() {
    const fbAuth = firebase.auth();
    //
    // return fbAuth.sendPasswordResetEmail('phuquy.uit@gmail.com')
    //   .then(() => console.log('email sent'))
    //   .catch((error) => console.log(error))
  }

  ngOnInit(){

  }

}
