import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

declare var $: any;
@Component({
  selector: 'app-menu-mobile',
  templateUrl: './menu-mobile.component.html',
  styleUrls: ['./menu-mobile.component.css']
})
export class MenuMobileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  closeMenu() {
    $('body').addClass('closeNav');
		$('body').removeClass('openNav');
  }

  routeToBlog() {
    this.closeMenu();
    this.router.navigate(['/blog']);
  }

  routeToLogin() {
    this.closeMenu();
    this.router.navigate(['/account/login']);
  }

  routeToRegister() {
    this.closeMenu();
    this.router.navigate(['/account/register']);
  }

  routeToProduct() {
    this.closeMenu();
    this.router.navigate(['/product']);
  }

  routeToHome() {
    this.closeMenu();
    this.router.navigate(['/']);
  }
}
