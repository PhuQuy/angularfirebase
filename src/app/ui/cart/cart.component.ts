import { Component, OnInit } from '@angular/core';
import { CartItem } from './../../models/cart-item';
import { ShoppingCart } from './../../models/shopping-cart';
import { ShoppingCartService } from './../../services/cart/shopping-cart.service';
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import { ChangeDetectionStrategy } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartComponent implements OnInit {
  cart: Observable<ShoppingCart>;
  cartItems: CartItem[];
  cartSubscription: Subscription;
  itemCount: number;

  constructor(private shoppingCartService: ShoppingCartService) { }

  public emptyCart(): void {
    this.shoppingCartService.empty();
  }

  removeProductFromCart(product: any): void {
    this.shoppingCartService.removeItem(product);
  }

  public ngOnInit(): void {
    this.cart = this.shoppingCartService.get();
    this.cartSubscription = this.cart.subscribe((cart) => {
      this.cartItems = cart.items;
      this.itemCount = cart.items.map((x) => x.quantity).reduce((p, n) => p + n, 0);
    });
  }

  public ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }

  close() {
    $("#dlDropDown").dropdown("toggle");
  }
}
