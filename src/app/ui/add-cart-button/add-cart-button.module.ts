import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCartButtonComponent } from './add-cart-button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AddCartButtonComponent],
  exports: [AddCartButtonComponent]
})
export class AddCartButtonModule { }
