import { Component, Input } from '@angular/core';
import { ShoppingCartService } from './../../services/cart/shopping-cart.service';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { Product } from './../../models/product';

@Component({
  selector: 'app-add-cart-button',
  templateUrl: './add-cart-button.component.html',
  styleUrls: ['./add-cart-button.component.css']
})
export class AddCartButtonComponent {
  @Input() product: any;

  added = false;

  public constructor(private shoppingCartService: ShoppingCartService) {

  }

  addCart(product: Product) {
    this.shoppingCartService.addItem(product, 1);
  }

  public productInCart(product: Product): boolean {
    return Observable.create((obs: Observer<boolean>) => {
      const sub = this.shoppingCartService
        .get()
        .subscribe(cart => {
          obs.next(cart.items.some(i =>i.product.id === product.id));
          obs.complete();
        });
      sub.unsubscribe();
    });
  }


  ngOnInit() {
  }

}
