import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxCarouselModule } from 'ngx-carousel';
import { BlogSliderComponent } from './blog-slider.component';
import { ProductItemModule } from './../product-item/product-item.module';
import { LoadingSpinnerModule } from './../loading-spinner/loading-spinner.module';

@NgModule({
  imports: [
    CommonModule,
    NgxCarouselModule,
    ProductItemModule,
    LoadingSpinnerModule
  ],
  declarations: [BlogSliderComponent],
  exports: [BlogSliderComponent]
})
export class BlogSliderModule { }
