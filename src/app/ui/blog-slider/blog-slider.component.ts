import { Component, OnInit, Output, ViewChild, Input, EventEmitter, ElementRef } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { Router } from "@angular/router";
import { SharedService } from './../../services/shared.service';
import { Blog } from './../../models/blog';

@Component({
  selector: 'app-blog-slider',
  templateUrl: './blog-slider.component.html',
  styleUrls: ['./blog-slider.component.css']
})
export class BlogSliderComponent implements OnInit {
  @ViewChild('prevButton') prevButton: ElementRef;
  @ViewChild('nextButton') nextButton: ElementRef;
  @Input() title: string = 'No title';
  @Input() blogs: any;
  @Input() isBlog: boolean;

  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;


  showSpinner = true;
  isSpecial = false;
  destroyMan: any;

  @Output() updateBlog = new EventEmitter<Blog>();

  constructor(protected router: Router, protected sharedService: SharedService) {
  }

  ngOnInit() {

    this.destroyMan = this.blogs.subscribe(products => {
      this.showSpinner = false;
      this.carouselTileItems = products;
    })

    this.carouselTile = {
      grid: { xs: 1, sm: 2, md: 2, lg: 3, all: 0 },
      slide: 2,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: true
      },
      load: 2,
      touch: true,
      easing: 'ease'
    }
  }

  readMore(blog: Blog) {
    this.sharedService.setKey(blog.id);
    this.router.navigate(['/blog', blog.slug]);
    this.updateBlog.emit(blog);
  }

  ngOnDestroy() {
    this.destroyMan.unsubscribe();
  }

  prev() {
    this.prevButton.nativeElement.click();
  }

  next() {
    this.nextButton.nativeElement.click();
  }

  details(product: any) {
    this.router.navigate(['/product', product.id]);
  }
}
