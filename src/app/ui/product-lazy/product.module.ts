import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { BigDealModule } from './../big-deal/big-deal.module';
import { ProductItemModule } from './../product-item/product-item.module';
import { PaginationModule } from './../pagination/pagination.module';
import { AddCartButtonModule } from './../add-cart-button/add-cart-button.module';
import { ProductImageSliderModule } from './../product-image-slider/product-image-slider.module';
import { BlogSliderModule } from './../blog-slider/blog-slider.module';
import { SmaillProductModule } from './../smaill-product/smaill-product.module';

import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routes: Routes = [
  {
    path: '', component: ProductComponent,
      children: [
        { path: '', component: ProductListComponent },
        { path: 'category/:key', component: ProductListComponent },
        { path: ':key', component: ProductDetailComponent }
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BigDealModule,
    ProductItemModule,
    Ng2OrderModule,
    BlogSliderModule,
    Ng2SearchPipeModule,
    AddCartButtonModule,
    NgxPaginationModule,
    ProductImageSliderModule,
    PaginationModule,
    SmaillProductModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductComponent, ProductListComponent, ProductDetailComponent]
})
export class ProductModule { }
