import { Component, OnInit } from '@angular/core';
import { ProductService } from './../../../services/product/product.service';
import { SharedService } from './../../../services/shared.service';
import { CategoryService } from './../../../services/categories/category.service';

import { Router } from "@angular/router";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ProductService, CategoryService]
})
export class ProductComponent implements OnInit {
  organic_goods: any;
  showSpinner = true;
  title_latest = "Mới nhất";
  today_deal: any;
  categories: any;
  destroyMan: any;

  constructor(private productService: ProductService, private router: Router,
    private sharedService: SharedService, private categoryService: CategoryService) {
    this.organic_goods = this.productService.getLimits(5);
    this.categories = this.categoryService.getCategories();
    sharedService.setKey('');
  }

  getKey(): string {
    return this.sharedService.getKey();
  }

  ngOnInit() {
    this.destroyMan = this.organic_goods.subscribe(x => {
      this.today_deal = x[0];
    });
  }

  ngOnDestroy() {
    if(this.destroyMan) {
      this.destroyMan.unsubscribe();
    }
  }

  reRoot() {
    this.router.navigate(['/product']);
    this.sharedService.setKey('');
  }

  heading() {
    return this.sharedService.getHeading();
  }
}
