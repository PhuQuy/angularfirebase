import { Component, OnInit } from '@angular/core';
import { ProductService } from './../../../services/product/product.service';
import { Product } from './../../../models/product';
import { Router, ActivatedRoute } from "@angular/router";
import { PaginationInstance } from 'ngx-pagination';
import { SharedService } from './../../../services/shared.service';
import { SeoService } from './../../../services/seo/seo.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [ProductService]
})
export class ProductListComponent implements OnInit {
  products: any;
  showSpinner = true;
  term: string;
  bottom = false;
  productRef: any;

  public config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 6,
    currentPage: 1
  };

  key: string = 'title'; //set default
  reverse: boolean = false;
  filter: any;

  sort(key) {
    var array = key.split(";");
    if (array[1] === 'true') {
      this.reverse = true;
    } else {
      this.reverse = false;
    }
    this.key = array[0];
    // console.log(this.reverse);
  }

  constructor(private productService: ProductService, private router: Router,  private seo: SeoService,
    private activatedRoute: ActivatedRoute, private sharedService: SharedService) {
    sharedService.setHeading('');
    // this.filter = 'c';
    sharedService.emitSearch$.subscribe(search => {
      this.filter = search;
    })
    this.activatedRoute.params.subscribe(params => {
      if (params['key']) {
        this.products = productService.getProductByCategory(params['key']);
        sharedService.setKey(params['key']);
      } else {
        this.products = this.productService.getProductsList();

        sharedService.setHeading('');
        this.sharedService.emitSearch$.subscribe(
          text => {
            this.term = text;
          });
      }
    });
  }

  ngOnInit() {
    this.products.subscribe(x => {
      this.showSpinner = false;
    });

    this.seo.generateTags({
      title: 'Hoa quả sạch',
      description: 'Hoa quả sạch ký túc xá',
      image: 'https://firebasestorage.googleapis.com/v0/b/rauquagiamcan.appspot.com/o/uploads%2Flogos.webp?alt=media&token=7cedfa39-8725-4e3d-a522-69086b9cb41b',
      slug: 'product'
    })
  }

  details(product: Product) {
    this.router.navigate(['/product', product.id]);
  }

}
