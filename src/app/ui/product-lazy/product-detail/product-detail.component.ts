import { Component } from '@angular/core';
import { ProductService } from './../../../services/product/product.service';
import { CommentService } from './../../../services/comment/comment.service';
import { ActivatedRoute } from "@angular/router";
import { Product } from './../../../models/product';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { SharedService } from './../../../services/shared.service';
import { SeoService } from './../../../services/seo/seo.service';
import { Comment } from './../../../models/comment';
import { PaginationInstance } from 'ngx-pagination';
import { AngularFireAuth } from 'angularfire2/auth';

declare var $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
  providers: [CommentService]
})
export class ProductDetailComponent {
  product: any;
  relatedProducts: any;
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;
  currentImage: string;
  productRef: any;
  comment: Comment = new Comment();
  comments: any = [];
  id: string;
  showSpinner = true;
  isloading = true;
  notFound = false;
  config: PaginationInstance = {
    id: 'comment',
    itemsPerPage: 6,
    currentPage: 1
  };

  destroyMan: any;

  loading() {
    this.isloading = false;
  }

  constructor(private afAuth: AngularFireAuth, private productService: ProductService, private seo: SeoService,
    private router: ActivatedRoute, private commentService: CommentService, private sharedService: SharedService) {
    this.relatedProducts = productService.getProductsList();
    this.router.params.subscribe(params => {
      $('body,html').animate({
        scrollTop: 0
      }, 500, 'swing');
      this.id = params['key'];
      this.productRef = productService.getProduct(this.id).subscribe(product => {
        this.showSpinner = false;
        if(!product) {
          this.notFound = true;
          return;
        }
        this.product = product;
        this.seo.generateTags({
          title: product.title,
          description: product.description,
          image: product.photoUrl,
          slug: 'product/' + this.id
        })

        this.seo.genrateTitle(product.title);
        this.destroyMan = commentService.getCommentByProduct(this.id).subscribe(comments => this.comments = comments);
        // console.log(this.productComments.valueChanges());
        sharedService.setHeading(product.title);
        this.product.photos.unshift(this.product.photoUrl);
        this.currentImage = product.photoUrl;
      });
    });
  }

  ngOnDestroy() {
    if(this.destroyMan) {
      this.destroyMan.unsubscribe();
    }

    if(this.productRef) {
      this.productRef.unsubscribe();
    }
  }

  selectImage(image) {
    if(this.currentImage === image) {
      return;
    }
    this.currentImage = image;
    this.isloading = true;
  }

  getTitle(): string {
    return 'Sản phẩm liên quan: ';
  }

  createComment() {
    this.comment.productID = this.id;
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.comment.userID
      }
      this.commentService.addComment(this.comment).then(response => {
        this.comment = new Comment();
      });
    });
  }

  ngOnInit() {
  }

}
