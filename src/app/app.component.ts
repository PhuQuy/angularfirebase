import { Component, AfterViewChecked } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewChecked {
  stylesLoaded = false;
  constructor() {
  }

  moveTop() {
    $('body,html').animate({
      scrollTop: 0
    }, 800, 'swing');
  }

  ngAfterViewChecked() {
    this.stylesLoaded = true;
    // require("style-loader!./../styles.css");
  }
}
